<?php
namespace Divecheck\Widget;

use Zend\View\Model\ViewModel;
use Zend\Http\Request;
use Zend\Http\Response;

interface WidgetInterface
{

    /**
     *
     * @return WidgetConfigInterface
     */
    public function getWidgetConfig();

    /**
     *
     * @param WidgetConfigInterface $config
     */
    public function init(WidgetConfigInterface $config);

    /**
     *
     * @return ViewModel
     * @throws Exception\WidgetException if the widget has problems fulfilling the request
     */
    public function dispatchAction(Request $request, Response $response);

    /**
     *
     */
    public function viewAction();

    /**
     *
     */
    public function editAction();

    /**
     *
     */
    public function helpAction();
}
