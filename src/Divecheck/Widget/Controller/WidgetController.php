<?php
namespace Divecheck\Widget\Controller;

use Sirrus\Mvc\Controller\AbstractActionController;
use Zend\Http\Response;

class WidgetController extends AbstractActionController
{
    /**
     * (non-PHPdoc)
     * @see \Zend\Mvc\Controller\AbstractActionController::createHttpNotFoundModel()
     */
    protected function createHttpNotFoundModel(Response $response)
    {
        $model = parent::createHttpNotFoundModel($response);
        $model->setVariable('content', 'Widget action not available');
        return $model;
    }

    public function indexAction()
    {

        $widgetAction = $this->params('widgetAction');
        $widgetName = $this->params('widgetName');

        /* @var $widget \Divecheck\Widget\WidgetInterface */
        $widget = $this->getServiceLocator()->get($widgetName);
        $widget->getControllerName();


        return $this->notFoundAction();
    }
}
