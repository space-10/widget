<?php
namespace Divecheck\Widget;

abstract class AbstractWidget implements WidgetInterface, WidgetConfigInterface
{
    protected $config;

    protected $initParameter;

    protected $widgetName;

    protected $displayName;

    protected $info;

    protected $supportedActions;

    /**
     */
    public function editAction()
    {

    }

    /**
     */
    public function helpAction()
    {

    }
}
