<?php
namespace Divecheck\Widget;

interface WidgetConfigInterface
{

    /**
     * Returns init parameter
     *
     * @param string $name
     * @return mixed
     */
    public function getInitParameter($name);

    /**
     * Returns list of parameter names
     *
     * @return array
     */
    public function getInitParameterNames();

    /**
     * Returns the name of the widget.
     *
     * @return string
     */
    public function getWidgetName();

    /**
     * Returns the description of the widget.
     *
     * @return string
     */
    public function getWidgetDescription();

    /**
     * Returns the display name of the widget.
     *
     * @return string
     */
    public function getDisplayName();

    /**
     * @return mixed
     */
    public function getWidgetInfo();

    /**
     * @return array
     */
    public function getSupportedActions();
}
