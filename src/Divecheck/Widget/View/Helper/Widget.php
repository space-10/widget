<?php
namespace Divecheck\Widget\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractPluginManager;
use Divecheck\Widget\WidgetInterface;
use Zend\View\Model\ViewModel;

class Widget extends AbstractHelper implements ServiceLocatorAwareInterface
{
    const DEFAULT_WIDGET_TEMPLATE = 'divecheck/widget/widget';
    /**
     *
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {

        if ($serviceLocator instanceof AbstractPluginManager)
        {
            $serviceLocator = $serviceLocator->getServiceLocator();
        }
        $this->serviceLocator = $serviceLocator;

        return $this;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
     */
    public function getServiceLocator()
    {

        return $this->serviceLocator;
    }


    /**
     *
     * @param string $widget
     * @return \Divecheck\Widget\View\Helper\Widget
     */
    public function __invoke($widget)
    {

        return $this;
    }

    protected function renderWidget(WidgetInterface $widget = null) {
        $model = new ViewModel();
        $model->setTemplate(static::DEFAULT_WIDGET_TEMPLATE);
        return $this->getView()->render($model);
    }

    public function __toString() {
        return $this->renderWidget();
    }
}
