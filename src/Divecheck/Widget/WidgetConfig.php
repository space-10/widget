<?php
namespace Divecheck\Widget;

class WidgetConfig implements WidgetConfigInterface
{
    protected $name;

    public function __construct($name, array $initParameter = [], array $options = [])
    {

    }

    public function getWidgetDescription()
    {

    }

    public function getSupportedActions()
    {

    }

    public function getDisplayName()
    {

    }

    public function getWidgetInfo()
    {

    }

    public function getWidgetName()
    {
        return $this->name;
    }

    public function getInitParameter($name)
    {
        return isset($this->initParameter[$name]) ? $this->initParameter[$name] : null;
    }

    public function getInitParameterNames()
    {
        return array_keys($this->initParameter);
    }
}
