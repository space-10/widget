<?php
namespace Divecheck\Widget;
return [
    'asset_manager' => include __DIR__ . DS . 'asset.config.php',
    'controllers' => [
        'invokables' => [
            'Divecheck\Widget\Controller\Widget' => 'Divecheck\Widget\Controller\WidgetController'
        ],
    ],
    'router' => [
        'routes' => [
            'widget' => [
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route' => '/widget[s]/:widgetName/:widgetAction',
                    'defaults' => [
                        'controller' => 'Divecheck\Widget\Controller\Widget',
                        'action' => 'index',
                        'widgetName' => 'Divecheck\Widget\DefaultWidget',
                        'widgetAction' => 'view'
                    ]
                ]
            ],
        ]
    ],
    'view_helpers' => [
        'invokables' => [
            'widget' => 'Divecheck\Widget\View\Helper\Widget',
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'divecheck/widget' => __DIR__ . '/../view/divecheck/widget/widget.phtml',
        ],
        'template_path_stack' => [
            /* 'core' => */
            __NAMESPACE__ => __DIR__ . '/../view'
        ]
    ]
];