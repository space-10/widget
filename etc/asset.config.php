<?php
namespace Divecheck\Widget;

return [
    'resolver_configs' => [
        'collections' => [

            'assets/admin/css/style.css' => [
                'css/widget.css'
            ]
        ],
        'paths' => [
            __DIR__ . '/../resources'
        ]
    ]
];